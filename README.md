# Gitlab ci-cd

## What is Gutlab ci-cd ?
- git lab cicd is process to autmoate the build test nad deploy using the gitlab.
- It is silimiar to Jenkins.
- We use differnet tools such as Maven,SonarQube,Tomcat, ansible etc.

# Gitlab pipeline has two main component
- Job describe the task that you need to do.
- Stage define order in which jobs will be completed.

## Pipeline 
- A pipeline is nothing but the set of instruction.

## GitLab Runner.
- A program that can run on your local host, VM, Containr (Jenkins Agent).
- Runners are processes that pick up and execute CI/CD jobs for GitLab. How do I configure runners?
- Nevigate setting -> CI/CD -> Runner

## How to start with the gitlab ci-cd pipeling

---
- Nevigate to CI_CD -> Editor -> Configure a ci/cd pipeline.
...

## Lets see the Sample.yml file
---
    stages:          # List of stages for jobs, and their order of execution
    - build
    - test
    - deploy

    build-job:       # This job runs in the build stage, which runs first.
    stage: build
    script:
        - echo "Compiling the code..."
        - echo "Compile complete."

    unit-test-job:   # This job runs in the test stage.
    stage: test    # It only starts when the job in the build stage completes successfully.
    script:
        - echo "Running unit tests... This will take about 60 seconds."
        - sleep 60
        - echo "Code coverage is 90%"

    lint-test-job:   # This job also runs in the test stage.
    stage: test    # It can run at the same time as unit-test-job (in parallel).
    script:
        - echo "Linting code... This will take about 10 seconds."
        - sleep 10
        - echo "No lint issues found."

    deploy-job:      # This job runs in the deploy stage.
    stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
    script:
        - echo "Deploying application..."
        - echo "Application successfully deployed."
***

## Few Modules used in gatlab ci-cd  Artificats, image ? 

- Artifacts are used to pass the output of one job to another, hence all tha joba are exicuted on the differnet environment.

---
    deploy-job:      # This job runs in the deploy stage.
        stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
        script:
            - echo "Deploying application..."
            - echo "Application successfully deployed."
            - python --version
        artifacts:
            path:
            - build/deploy.txt
***

- Image module is used whean we want a docer image in our script.

---
    image: python    # user for python docker image 
    deploy-job:      # This job runs in the deploy stage.
        stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
        script:
            - echo "Deploying application..."
            - echo "Application successfully deployed."
***

- Variables in gitlabcicd

---
    varibles:   
        username: abhalkar
***

- [go in setting] Nevigate Settings -> CI/CD -> variables -> ad variable -> key: password -> value: mypassword -> [Select] protected [Select] masked

- Password: Value    -----> Key: value(masked)




